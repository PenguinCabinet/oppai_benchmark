[REAMDME OF ENGLISH](./Readme_en.md)
# おっぱい関数積分ベンチマーク

![Oppai](https://github.com/PenguinCabinet/Oppai_benchmark/raw/master/explanation/Oppai.gif)

これは揺れるおっぱい関数を積分して、その速度を計測するベンチマークソフトです。 

[おっぱい関数は「大学対抗全日本おっぱい関数選手権大会」エントリー作品 名古屋大学代表](https://www.desmos.com/calculator/i05puaquwh?lang=ja)のものを使用させていただきました。


# ダウンロード
[ダウンロード](https://github.com/PenguinCabinet/Oppai_benchmark/releases/latest)

# 使い方

## GUI
実行ファイルをクリックするか\
コマンドラインで
```shell
Oppai_benchmark
```

## CUI
コマンドラインで
```shell
Oppai_benchmark c
```


# 性能の参考値

CPUスペック | スコア 
--- | ---
Linode 48コアインスタンス | 26175
Ryzen9 3950X(win10) | 24000
Core i7-12700KF|21236
Ryzen9 3900X(win10)| 21000
Ryzen9 3950X(ubuntu20.04) | 20000
Ryzen7 5800HS(win10)| 10000
Ryzen7 1700(win10) | 8004
Core i7 9700F(win10)| 7500
Ryzen5 2600(win10) |6778
Core i5-8250U(win10)|2900
Atom x5-Z8550(win10)|870
Pentium N4200(win10)|860
